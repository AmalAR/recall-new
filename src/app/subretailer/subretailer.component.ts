import { Component, OnInit,ViewChild } from '@angular/core';
import {MatTableDataSource,MatSort,MatPaginator} from '@angular/material';
import { Router } from '@angular/router';

export interface PeriodicElement {
name: string;
position: number;
contact: number;
email: string;
address:string;
sellername:string;
productname:string;
action:string;
}

const ELEMENT_DATA: PeriodicElement[] = [
{position: 1, name: "hifzur", contact: 7200348034, email: "demo@gmail.com", address: "Chennai" ,sellername:"adyardepo",productname:"dell", action:"power"},
{position: 2, name: "amal", contact: 5556662222, email: "demo@gmail.com", address: "Chennai" ,sellername:"adyardepo",productname:"dell", action:"power"},
{position: 3, name: "sam", contact: 5585545525, email: "demo@gmail.com", address: "Chennai" ,sellername:"adyardepo",productname:"dell", action:"power"},
{position: 4, name: "dots", contact: 7200348034, email: "demo@gmail.com", address: "Chennai" ,sellername:"adyardepo",productname:"dell", action:"power"},
{position: 5, name: "kavi", contact: 7200348034, email: "demo@gmail.com", address: "Chennai" ,sellername:"adyardepo",productname:"dell", action:"power"},
{position: 6, name: "fravez", contact: 7200348034, email: "demo@gmail.com", address: "Chennai" ,sellername:"adyardepo",productname:"dell", action:"power"},

];

@Component({
  selector: 'app-subretailer',
  templateUrl: './subretailer.component.html',
  styleUrls: ['./subretailer.component.scss']
})
export class SubretailerComponent implements OnInit {
   
  recentrecalls: any[];
  recallcards: {}[];
  displayedColumns: string[] = ['position', 'name', 'contact', 'email' ,'address','sellername','productname','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(
    public router:Router
  ) { }
  
  ngOnInit() {
  this.dataSource.sort = this.sort;
  this.dataSource.paginator = this.paginator;
  
  }
  view(){
       this.router.navigate(['/subretailer']);
  }
}
