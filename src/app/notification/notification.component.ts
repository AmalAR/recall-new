import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  notificationlist:any;
  constructor() { }

  ngOnInit() {
    this.notificationlist = [
      {id:'12',productimg:'boy.png',price:1750,orderno:'OD212332120537538000',productname:'The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. ',deliverydate:'May 24th \'18'},
      {id:'12',productimg:'boy.png',price:12750,orderno:'OD212332120537538000',productname:'The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. ',deliverydate:'May 24th \'18'},
      {id:'12',productimg:'boy.png',price:1750,orderno:'OD212332120537538000',productname:'The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. ',deliverydate:'May 24th \'18'},
      {id:'12',productimg:'boy.png',price:41750,orderno:'OD212332120537538000',productname:'The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. ',deliverydate:'May 24th \'18'},
    ]
  }

}
