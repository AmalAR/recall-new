import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-supplier-register',
  templateUrl: './supplier-register.component.html',
  styleUrls: ['./supplier-register.component.scss']
})
export class SupplierRegisterComponent implements OnInit {

  constructor(
    public router :Router
  ) { }

  ngOnInit() {
  }
  loginClick(){
    this.router.navigate(['/manufacturer-login']);
    }
}
