import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminrecallprocessComponent } from './adminrecallprocess.component';

describe('AdminrecallprocessComponent', () => {
  let component: AdminrecallprocessComponent;
  let fixture: ComponentFixture<AdminrecallprocessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminrecallprocessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminrecallprocessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
