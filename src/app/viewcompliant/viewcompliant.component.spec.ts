import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewcompliantComponent } from './viewcompliant.component';

describe('ViewcompliantComponent', () => {
  let component: ViewcompliantComponent;
  let fixture: ComponentFixture<ViewcompliantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewcompliantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewcompliantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
