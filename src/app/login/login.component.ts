import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public  loginbg="loginbg.jpg"
  email: any;
  hide = true;
  public logo="logolog.png"
  password: string;
  emailid: any;
  pass: any;
  userdetail: any;
  constructor(
    public router:Router,
    public userService: UserService
  ) { }

  ngOnInit() {
  }
  registerClick(){
    this.router.navigate(['/register'])
  }
  manufacturerregisterClick(){
    this.router.navigate(['/manufacturerregister'])
  }


  logintohome(data1,data2){

    console.log(data1,data2)
this.email="user@gmail.com";
this.password="123456";
if(this.email==data1 && this.password==data2)

{
    this.router.navigate(['/dashboard'])  
  }
else {

  alert("wrong credential")
}
}
login(){
  console.log(this.emailid)
  console.log(this.pass)
this.userService.loginUser(this.emailid,this.pass).pipe(first()).subscribe((res:any)=>{
console.log(res);
  this.userdetail = res.payload.user1[0];
  console.log(this.userdetail);
  if(res.payload.user1[0].type==1){
    this.router.navigate(['/dashboard']);
  }
  else{
    this.router.navigate(['/manufacturerdashboard']);
  }
  localStorage.setItem('userinfo',JSON.stringify(this.userdetail));
});
 
}

}