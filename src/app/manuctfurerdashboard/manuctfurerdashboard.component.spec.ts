import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuctfurerdashboardComponent } from './manuctfurerdashboard.component';

describe('ManuctfurerdashboardComponent', () => {
  let component: ManuctfurerdashboardComponent;
  let fixture: ComponentFixture<ManuctfurerdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuctfurerdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuctfurerdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
