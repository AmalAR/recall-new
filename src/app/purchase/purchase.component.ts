import { Component, OnInit, ɵConsole } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { UserService } from '../user.service';
import { first } from 'rxjs/operators';
import * as moment from 'moment';
@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent implements OnInit {
  recentrecalls: any;
  mid=0;
  Manufacturer:any;
  vehiclemodelname:any;
  modelyear:any;
  vin:any;
  createtype=1;
  dateofpurchase:any;
  retailername:any;
  paymentmode:any;  
  productname:any;
  referenceno:any;
  payment:any;
  retailerlocation:any;
  manufacturerdate:any;
  dateofexpiry:any;
  notes:any;
  modelname:any;
  categoryid:any;
  newsupdatelist: any[];
  newsupdatelistconsumer:any;
  newsupdatelistfood:any;
  newsupdatelistdrug:any;


  currentmodal: any;
  consumerhidden=true;
  motorhidden=true;
  drugshidden=true;
  foodhidden=true;
  motorproduct: any[];
  addproductlist=[];
  addprodetail=[];
  deletepro: any;
  category:any;
  updtadeProduct: any;
  userid:any;
  userinfo:any;
  productlist:any;
  public editproshow=false;
  filterdata=[];
  vinfrom: any;
  vinto: any;
  constructor(public userService: UserService) {
    this.addproductlist = JSON.parse(localStorage.getItem('Manufacturer'));

   }

  ngOnInit() {
    this.userinfo=JSON.parse(localStorage.getItem('userinfo'));
   
    console.log(this.userinfo);
    this.userid=this.userinfo.id;
    console.log("userid",this.userid);

    this.userService.listCategorySelect().subscribe((res:any)=>{
      this.category = res.payload.user;
      console.log(this.category);
    
    });

    this.userService.listproduct(this.userid,0).subscribe((res:any)=>{
      this.productlist=res.payload.user;
      console.log(this.productlist[0].dateofpurchase);

    });

    this.newsupdatelist = [
      {newstitle:'MITSUBISHI ( 19V330000 )',newscontent:'Dated: APR 26, 2019 Mitsubishi Motors North Americ...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V330000&refurl=rss'},
      {newstitle:'HONDA ( 19V315000 )',newscontent:'Dated: APR 18, 2019 Honda (American Honda Motor Co...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V315000&refurl=rss'},
      {newstitle:'VOLVO ( 19V327000 )',newscontent:'Dated: APR 25, 2019 Volvo Car USA LLC (Volvo) is r...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V327000&refurl=rss'},
      {newstitle:'FREIGHTLINER ( 19V309000 )',newscontent:'Dated: MAY 10, 2019 Forest River, Inc. (Forest Riv...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V309000&refurl=rss'},
      {newstitle:'LIVI\' LITE ( 19V307000 )',newscontent:'Dated: APR 17, 2019 Livin\' Lite Recreational Vehic...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V327000&refurl=rss'},
      {newstitle:'LIVIN LITE ( 19V307000 )',newscontent:'Dated: APR 17, 2019 Livin\' Lite Recreational Vehic...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V307000&refurl=rss'},
      {newstitle:'MAZDA ( 19V323000 )',newscontent:'Dated: APR 25, 2019 Mazda North American Operation...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V323000&refurl=rss'},
      {newstitle:'SPENCER ( 19V305000 )',newscontent:'Dated: APR 17, 2019 Spencer Manufacturing, Inc. is...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V305000&refurl=rss'},
      {newstitle:'UTILIMASTER ( 19V304000 )',newscontent:'Dated: APR 17, 2019 Spartan Motors USA (Spartan) i...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V304000&refurl=rss'}
    ]
    this.newsupdatelistconsumer = [
      {newstitle:'Target Recalls USB Charging Cables Due to Shock and Fire Hazards',newscontent:'The metal around the cord can become electrically ...',path:'https://www.cpsc.gov/Recalls/2019/target-recalls-usb-charging-cables-due-to-shock-and-fire-hazards'},
      {newstitle:'Aegean Apparel Recalls Children’s Sleepwear Due to Violation of Federal Flammability Standard',newscontent:'The children’s sleepwear garments fail to meet the...',path:'https://www.cpsc.gov/Recalls/2019/aegean-apparel-recalls-childrens-sleepwear-due-to-violation-of-federal-flammability'},
      {newstitle:'Children’s Sleep Sacks Recalled by Gildan Activewear Due to Violation of Federal Flammability Standard; Sold Exclusively at AmericanApparel.com (Recall Alert)',newscontent:'The children’s sleep sacks fail to meet the flamma...',path:'https://www.cpsc.gov/Recalls/2019/childrens-sleep-sacks-recalled-by-gildan-activewear-due-to-violation-of-federal'},
      {newstitle:'DICK’S Sporting Goods Recalls Ethos Pull-Up Assist Due To Laceration Hazard',newscontent:'The plastic clip on the nylon web band that is att...',path:'https://www.cpsc.gov/Recalls/2019/dicks-sporting-goods-recalls-ethos-pull-up-assist-due-to-laceration-hazard'},
      {newstitle:'Far East Brokers Recalls Pineapple Corer & Slicers Due to Laceration Hazard',newscontent:'The metal blade of the slicer can detach, posing a...',path:'https://www.cpsc.gov/Recalls/2019/far-east-brokers-recalls-pineapple-corer-and-slicers-due-to-laceration-hazard'},
      {newstitle:'Revelate Designs Recalls Bicycle Seat Bags Due to Crash, Injury Hazards',newscontent:'The straps that fasten the bag to the bike seat ca...',path:'https://www.cpsc.gov/Recalls/2019/revelate-designs-recalls-bicycle-seat-bags-due-to-crash-injury-hazards'},
      {newstitle:'Arctic Cat Recalls Textron Recreational Off-Highway Vehicles Due to Crash Hazard (Recall Alert)',newscontent:'The upper front suspension arms can fail, posing a...',path:'https://www.cpsc.gov/Recalls/2019/arctic-cat-recalls-textron-recreational-off-highway-vehicles-due-to-crash-hazard-recall'},
      {newstitle:'ION Audio Recalls Portable Speakers Due to Explosion Hazard',newscontent:'Hydrogen gas can leak from the portable speaker ba...',path:'https://www.cpsc.gov/Recalls/2019/ion-audio-recalls-portable-speakers-due-to-explosion-hazard'},
      {newstitle:'Meijer Recalls French Fry Cutters Due to Laceration Hazard',newscontent:'The french fry cutter’s blade can detach, posing a...',path:'https://www.cpsc.gov/Recalls/2019/meijer-recalls-french-fry-cutters-due-to-laceration-hazard'},
      {newstitle:'Beaba Recalls Baby Food Steam Cooker/Blenders Due to Laceration Hazard',newscontent:'The glass bowl can break, posing a laceration haza...',path:'https://www.cpsc.gov/Recalls/2019/Beaba-Recalls-Baby-Food-Steam-Cooker/Blenders-Due-to-Laceration-Hazard'}
    ]
    this.newsupdatelistfood = [
      {newstitle:'J Deluca Fish Company Inc. Recalls Siluriformes Products Produced Without Benefit of Import Inspection',newscontent:'J Deluca Fish Company Inc., doing business as Naut...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-061-2019-release'},
      {newstitle:'Richwell Group, Inc. Recalls Siluriformes Products Produced Without Benefit of Import Inspection',newscontent:'Richwell Group, Inc. Recalls Siluriformes Products Produced Without Benefit of Import Inspection',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-060-2019-release'},
      {newstitle:'Sikorski Sausages Co., Ltd. Recalls Meat and Poultry Sausage Products Produced without Benefit of Import Inspection',newscontent:'Sikorski Sausages Co., Ltd., a London, Ontario, Ca...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-059-2019-release'},
      {newstitle:'Blount Fine Foods Recalls Soup with Chicken Products Due to Possible Foreign Matter Contamination',newscontent:'Blount Fine Foods, a McKinney, Texas establishment...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-058-2019-release'},
      {newstitle:'Aurora Packing Company, Inc. Recalls Beef Products Due to Possible E. coli O157:H7 Contamination',newscontent:'Aurora Packing Company, Inc., a North Aurora, Ill....',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-057-2019-release'},
      {newstitle:'BEF Foods, Inc. Recalls Beef Products Produced without Benefit of Inspection',newscontent:'BEF Foods, Inc., a Lima, Ohio establishment, is re...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-056-2019-release'},
      {newstitle:'Vienna Beef Ltd. Recalls Beef Products due to Possible Foreign Matter Contamination',newscontent:'Vienna Beef Ltd., a Chicago, Ill. establishment, i...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-055-2019-release'},
      {newstitle:'Caito Foods LLC. Recalls Salads with Chicken Products due to Misbranding and Undeclared Allergens',newscontent:'Caito Foods LLC., an Indianapolis, Ind. establish...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-055-2019-release'},
      {newstitle:'MIBO Fresh Foods LLC Recalls Salad with Meat Products due to Misbranding and Undeclared Allergens',newscontent:'MIBO Fresh Foods LLC, a Fort Worth, Texas establis...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-055-2019-release'},
      {newstitle:'Crab House Trading Corp. Recalls Siluriformes Products Produced without Benefit of Inspection',newscontent:'Crab House Trading Corp., a Los Angeles, Calif. es...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-055-2019-release'},
    ]
    this.newsupdatelistdrug = [
      {newstitle:'E-cigarette: Safety Communication - Related to Seizures Reported Following E-cigarette Use, Particularly in Youth and Young Adults',newscontent:'Safety Communication - Related to Seizures Reporte...',path:'https://www.fda.gov/safety/medwatch-safety-alerts-human-medical-products/e-cigarette-safety-communication-related-seizures-reported-following-e-cigarette-use-particularly'},
      {newstitle:'Certain Prescription Insomnia Medicines: New Boxed Warning - Due to Risk of Serious Injuries Caused by Sleepwalking, Sleep Driving and Engaging in Other Activities While Not Fully Awake',newscontent:'MedWatch Alert on certain prescription insomnia me...',path:'https://www.fda.gov/safety/medwatch-safety-alerts-human-medical-products/certain-prescription-insomnia-medicines-new-boxed-warning-due-risk-serious-injuries-caused'}
    ]


this.addproductlist = [
                    {
                        dateofexpiry: "2019-05-30",
                        manufacturerdate: "2019-05-31",
                        manufecturername: "AR",
                        notes: "This is Project is Aswome",
                        payment: "70000000",
                        paymentmode: "Credit Card",
                        productname: "Table",
                        producttype: "Consumer",
                        purchasedate: "2019-05-24",
                        referenceno: "Machines",
                        retailerlocation: "Chennai",
                        retailname: "Vani"
                    },
                    {
                      dateofexpiry: "2019-05-30",
                      manufacturerdate: "2019-05-31",
                      manufecturername: "AR",
                      notes: "This Product is manufucturered in Chinna",
                      payment: "70000000",
                      paymentmode: "Credit Card",
                      productname: "Watching Machines",
                      producttype: "Vechile",
                      purchasedate: "2019-05-24",
                      referenceno: "Machines",
                      retailerlocation: "UK",
                      retailname: "Sam"
                  },                    
]


  }


  filter(data){
    console.log(data)
    if(data==0){
      this.addproductlist = JSON.parse(localStorage.getItem('Manufacturer'));
      this.filterdata=[];
    }
    else{
      for(let x=0;x<this.addproductlist.length;x++){
        if(this.addproductlist[x].producttype==data){
          this.filterdata.push(this.addproductlist[x]);
        }
      }
      this.addproductlist=this.filterdata;
    }
  }



  myFunction(data) {
   
   console.log(data)
    this.currentmodal=data;
    console.log(this.currentmodal);
     if(data=="1"){
       this.consumerhidden=true;
       this.motorhidden = false;
       this.foodhidden = true;
       this.drugshidden = true;
     }
     if(data=="2"){
       this.motorhidden = true;
       this.consumerhidden=false;
     }
    //  if(data=="3"){
    //   this.motorhidden = false;
    //   this.consumerhidden=true;
    // }
    // if(data=="4"){
    //   this.motorhidden = false;
    //   this.consumerhidden=true;
    // }
   }
addproduct(){
      this.motorproduct = [
                              {
                                // categoryid: this.categoryid,
                                categoryid: this.currentmodal,
                                producttype: this.currentmodal,
                                manufecturername:this.Manufacturer,
                                vechilemodename:this.vehiclemodelname,
                                year:this.modelyear,
                                vin:this.vin,
                                purchasedate:this.dateofpurchase,
                                retailname:this.retailername,
                                paymentmode:this.paymentmode,
                                productname:this.productname,
                                referenceno:this.referenceno,
                                payment:this.payment,
                                createtype:this.createtype,
                                modelname:this.modelname,
                                retailerlocation:this.retailerlocation,
                                manufacturerdate:this.manufacturerdate,
                                dateofexpiry:this.dateofexpiry,
                                notes:this.notes
                              }
                          ];
                          console.log(this.modelname);
        this.addprodetail.push(this.motorproduct[0]);
        localStorage.setItem('Manufacturer',JSON.stringify(this.addprodetail));
        this.addproductlist = JSON.parse(localStorage.getItem('Manufacturer'));
        // categoryid,productname,referenceno,dateofpurchase,manufacturer,modelname,modelyear,vin,retailername,paymentmode,amount,createtype

                          console.log(this.categoryid);
                          // var year = this.modelyear
                          var year = moment(this.modelyear).format('YYYY');
                          console.log(year);
                          
        this.userService.addProduct(this.userid,this.mid,this.currentmodal,this.productname,this.referenceno,this.dateofpurchase,this.Manufacturer,this.modelname,year,this.vinfrom,'',this.retailername,this.paymentmode,this.payment,this.createtype)
        .pipe(first()).subscribe((res:any)=>{
            console.log(res);
            this.productlistnew()
        });
}
productlistnew(){
  this.userService.listproduct(this.userid,0).subscribe((res:any)=>{
    this.productlist=res.payload.user;
    console.log(res);
  
  });
}
   deleteproduct(data){
            for(var i=0;i<this.addproductlist.length;i++){
              console.log(this.addproductlist[i]);
                if(i===data){
                  this.addproductlist.splice(i,1);
                  localStorage.setItem('Manufacturer',JSON.stringify(this.addproductlist));
            }
            }
   }
   editproduct(data){
            this.editproshow=true;
            this.updtadeProduct = data;
   }

   saveProduct(index,productname,referenceno,purchasedate,payment,vechilemodename){
    this.editproshow=false;
    for(var i=0;i<this.addproductlist.length;i++){
      console.log("Inner Updated");
      console.log(this.addproductlist[i]);
      if(i==index){
        this.addproductlist[i].productname=productname;
        this.addproductlist[i].referenceno=referenceno;
        this.addproductlist[i].purchasedate=purchasedate;
        this.addproductlist[i].payment=payment;
        this.addproductlist[i].vechilemodename=vechilemodename;
      }
    } 
    localStorage.setItem('Manufacturer',JSON.stringify(this.addproductlist));
    this.editproshow=false;
   }



}
