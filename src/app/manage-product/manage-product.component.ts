// import { Component, OnInit, Inject } from '@angular/core';
// import {FormControl, FormGroup} from '@angular/forms';
// import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';


// export interface DialogData {
//   animal: string;
//   name: string;
// }



import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';


export interface PeriodicElement {
  position: number;
  // name: string;
  // position: number;
  // weight: number;
  // symbol: string;
  productid: number;
  path:string;
  procategory:string;
  procatemodel: any;
  productheader :any;
  productName :any;
  Manufacturer : any;
  ImporterWholesaler: any;
  RetailerName : any;
 RetailerPhone : any;
 PurchaseDate : any
  PaymentMode :any;
}

const ELEMENT_DATA: PeriodicElement[] = [
  // {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  // {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  // {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  // {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  // {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  // {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  // {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  // {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  // {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  // {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},

  {
    position: 1,
    productid: 1,
    path:'addrecall',
    procategory: 'Food product',
    procatemodel: 'model1',
    productheader : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    productName :'Product1',
    Manufacturer : 'TVS',
    ImporterWholesaler: 'kya',
    RetailerName : 'Anju',
    RetailerPhone : 9823478634,
    PurchaseDate : 22-21-2009,
    PaymentMode : 'Credit',
   },
   {
    position: 2,
    productid: 2,
    path:'addrecall',
    procategory: 'consumer product',
    procatemodel: 'model1',
    productheader : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    productName :'Product2',
    Manufacturer : 'TVS',
    ImporterWholesaler: 'kya',
    RetailerName : 'kumar',
    RetailerPhone : 9823478634,
    PurchaseDate : 22-21-2009,
    PaymentMode : 'Credit',
   },
   {
    position: 3,
    productid: 3,
    path:'addrecall',
    procategory: 'Food product',
    procatemodel: 'model1',
    productheader : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    productName :'Product3',
    Manufacturer : 'TVS',
    ImporterWholesaler: 'kya',
    RetailerName : 'Anju',
    RetailerPhone : 9823478634,
    PurchaseDate : 22-21-2009,
    PaymentMode : 'Credit',
   },



];





















@Component({
  selector: 'app-manage-product',
  templateUrl: './manage-product.component.html',
  styleUrls: ['./manage-product.component.scss'],

})

export class ManageProductComponent implements OnInit {
//   consumerhidden=true;
//   motorhidden = true;
//   reportingproblem = new FormGroup({
//     reportproblemcomment : new FormControl(''),
//   })
//   reportproblemcomment
//  products=[];
//  reportedproblem:any;
//   currentmodal: any;
//   reportdata: any;
//   viewproduct: any;

//   animal: string;
//   name: string;



//   constructor( public dialog : MatDialog) { }

//   openDialog(): void {
//     const dialogRef = this.dialog.open(ReportDialog, {
//       width: '250px',
//       data: {name: this.name, animal: this.animal}
//     });

//     dialogRef.afterClosed().subscribe(result => {
//       console.log('The dialog was closed');
//       this.animal = result;
//     });
//   }
//   ngOnInit() {
//     this.products=[
//       {
//         "productid": 1,
//         "procategory": "Food product",
//         "procatemodel": "model1",
//         "productheader" : "Motor Product",
//         "ProductName" :"Motor Product",
//         "Manufacturer" : "TVS",
//         "ImporterWholesaler": "kya",
//         "RetailerName" : "Anju",
//         "RetailerPhone" : "9823478634",
//         "PurchaseDate" : "22-21-2009",
//         "PaymentMode" : "Credit",
      
//       },
//       {
//         "productid": 2,
//         "procategory": "Motor vehicle",
//         "procatemodel": "model2",
//         "productheader" : "Consumer Product",
//         "ProductName" :"Consumer Product",
//         "Manufacturer" : "Hero",
//         "ImporterWholesaler": "kya",
//         "RetailerName" : "Dev",
//         "RetailerPhone" : "9823478634",
//         "PurchaseDate" : "22-21-2009",
//         "PaymentMode" : "Credit",
      
//       },
//       {
//         "productid": 3,
//         "procategory": "consumer product",
//         "productheader" : "consumer product",
//         "procatemodel": "model3",
//         "ProductName" : "Consumer Product",
//         "Manufacturer" : "Bajaj",
//         "ImporterWholesaler": "kya",
//         "RetailerName" : "Vibil",
//         "RetailerPhone" : "9823478634",
//         "PurchaseDate" : "22-21-2009",
//         "PaymentMode" : "Credit",
      
      
//       },
//       {
//         "productid": 4,
//         "procategory": "Drug",
//         "procatemodel": "model4",
//         "productheader" : "Motor Product",
//         "ProductName" : "Motor Product",
//         "Manufacturer" : "Honda",
//         "ImporterWholesaler": "kya",
//         "RetailerName" : "Kumar", 
//         "RetailerPhone" : "9823478634",
//         "PurchaseDate" : "22-21-2009",
//         "PaymentMode" : "Credit"
      
//       },

//   ]


//   }
  viewprod(data){
    this.viewproduct=data;
  }
//   reportpop(data){
//     this.reportdata=data;
//     console.log(this.reportdata);
//   }
//   onSubmit(data){
//     this.reportedproblem=data;
//     console.log(this.reportedproblem);
//     console.log(this.reportingproblem.value);
//   }
//   myFunction(data) {
   
//    this.currentmodal=data;
//     if(data=="Consumer"){

//       this.consumerhidden=false;
//       this.motorhidden = true;
//     }
//     if(data=="Motor"){
//       this.motorhidden = false;
//       this.consumerhidden=true;
 
//     }

//   }

// }
// @Component({
//   selector: 'report-dialog',
//   templateUrl: 'report-dialog.html',
// })
// export class ReportDialog {
//   constructor( public dialogRef: MatDialogRef<ReportDialog>,@Inject(MAT_DIALOG_DATA) public data: DialogData) {}
//   onNoClick(): void {
//     this.dialogRef.close();
//   }











products=[];
  
currentmodal: any;
viewproduct: any;
constructor(
 public router:Router
) { }

 ngOnInit() {



  
   this.products=[

 ];


 }


 displayedColumns: string[] = ['select', 'position', 'productName', 'productheader', 'PurchaseDate','RetailerName','action'];
 dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
 selection = new SelectionModel<PeriodicElement>(true, []);

 /** Whether the number of selected elements matches the total number of rows. */
 isAllSelected() {
   const numSelected = this.selection.selected.length;
   const numRows = this.dataSource.data.length;
   return numSelected === numRows;
 }

 /** Selects all rows if they are not all selected; otherwise clear selection. */
 masterToggle() {
   this.isAllSelected() ?
       this.selection.clear() :
       this.dataSource.data.forEach(row => this.selection.select(row));
 }

 /** The label for the checkbox on the passed row */
 checkboxLabel(row?: PeriodicElement): string {
   if (!row) {
     return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
   }
   return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
 }













 viewlist(data){
 this.viewproduct=data;
//  this.router.navigate(['/addrecall']);
     }
 consumerhidden=true;
 motorhidden = true;
 myFunction(data) {
  
  this.currentmodal=data;
   if(data=="Consumer"){

     this.consumerhidden=false;
     this.motorhidden = true;
   }
   if(data=="Motor"){
     this.motorhidden = false;
     this.consumerhidden=true;

   }

 }

 viewcomplaintClick(){
  this.router.navigate(['/viewcomplaint']);
 }
 



















}
