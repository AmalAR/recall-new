import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManufactureraddrecallComponent } from './manufactureraddrecall.component';

describe('ManufactureraddrecallComponent', () => {
  let component: ManufactureraddrecallComponent;
  let fixture: ComponentFixture<ManufactureraddrecallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManufactureraddrecallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManufactureraddrecallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
