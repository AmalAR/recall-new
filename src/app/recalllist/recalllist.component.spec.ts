import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecalllistComponent } from './recalllist.component';

describe('RecalllistComponent', () => {
  let component: RecalllistComponent;
  let fixture: ComponentFixture<RecalllistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecalllistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecalllistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
