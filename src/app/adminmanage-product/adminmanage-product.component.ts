import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { UserService } from '../user.service';
import { first } from 'rxjs/operators';
import * as moment from 'moment';

export interface PeriodicElement {
  position: number;
  // name: string;
  // position: number;
  // weight: number;
  // symbol: string;
  productid: number;
  path:string;
  procategory:string;
  procatemodel: any;
  productheader :any;
  productName :any;
  Manufacturer : any;
  ImporterWholesaler: any;
  RetailerNameaddress : any;
 RetailerPhone : any;
 PurchaseDate : any
  PaymentMode :any;
}

const ELEMENT_DATA: PeriodicElement[] = [
  // {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  // {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  // {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  // {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  // {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  // {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  // {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  // {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  // {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  // {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},

  {
    position: 1,
    productid: 1,
    path:'addrecall',
    procategory: 'Food product',
    procatemodel: 'model1',
    productheader : 'Motor Product',
    productName :'Motor Product',
    Manufacturer : 'TVS',
    ImporterWholesaler: 'kya',
    RetailerNameaddress : 'Anju, 45,Chennai',
    RetailerPhone : 9823478634,
    PurchaseDate : 22-21-2009,
    PaymentMode : 'Credit',
   },
   {
    position: 2,
    productid: 2,
    path:'addrecall',
    procategory: 'consumer product',
    procatemodel: 'model1',
    productheader : 'consumer Product',
    productName :'consumer Product',
    Manufacturer : 'TVS',
    ImporterWholesaler: 'kya',
    RetailerNameaddress : 'kumar, 45,Chennai',
    RetailerPhone : 9823478634,
    PurchaseDate : 22-21-2009,
    PaymentMode : 'Credit',
   },
   {
    position: 3,
    productid: 3,
    path:'addrecall',
    procategory: 'Food product',
    procatemodel: 'model1',
    productheader : 'Motor Product',
    productName :'Motor Product',
    Manufacturer : 'TVS',
    ImporterWholesaler: 'kya',
    RetailerNameaddress : 'Anju, 45,Chennai',
    RetailerPhone : 9823478634,
    PurchaseDate : 22-21-2009,
    PaymentMode : 'Credit',
   },



];
@Component({
  selector: 'app-adminmanage-product',
  templateUrl: './adminmanage-product.component.html',
  styleUrls: ['./adminmanage-product.component.scss']
})
export class AdminmanageProductComponent implements OnInit {

  products=[];
  public manulogo = 'ford-logo.png'
  currentmodal: any;
  viewproduct: any;
  userid=0;
  productlist: any;
  category: any;
  userinfo: any;
  Manufacturer: any;
  modelname: any;
  vinfrom: any;
  productname: any;
  referenceno: any;
  dateofpurchase: any;
  payment: any;
  paymentmode: any;
  retailername: any;
  modelyear: any;
  vinto: any;
  mid:any;
 constructor(
   public router:Router,
   public userService:UserService
 ) { }
 
   ngOnInit() {
    this.userinfo=JSON.parse(localStorage.getItem('userinfo'));
   
    console.log(this.userinfo);
    this.mid=this.userinfo.id;
    console.log("manuid", this.mid);

    this.userService.listCategorySelect().subscribe((res:any)=>{
      this.category = res.payload.user;
      console.log(this.category);
    
    });

    this.userService.listproduct(0,this.mid).subscribe((res:any)=>{
      this.productlist=res.payload.user;
      console.log(res);

    });
   }


   displayedColumns: string[] = ['select', 'position', 'productName', 'productheader', 'action'];
   dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
   selection = new SelectionModel<PeriodicElement>(true, []);
 
   /** Whether the number of selected elements matches the total number of rows. */
   isAllSelected() {
     const numSelected = this.selection.selected.length;
     const numRows = this.dataSource.data.length;
     return numSelected === numRows;
   }
 
   /** Selects all rows if they are not all selected; otherwise clear selection. */
   masterToggle() {
     this.isAllSelected() ?
         this.selection.clear() :
         this.dataSource.data.forEach(row => this.selection.select(row));
   }
 
   /** The label for the checkbox on the passed row */
   checkboxLabel(row?: PeriodicElement): string {
     if (!row) {
       return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
     }
     return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
   }













   viewlist(data){
   this.viewproduct=data;
  //  this.router.navigate(['/addrecall']);
       }
   consumerhidden=true;
   motorhidden = true;
   myFunction(data) {
   
    console.log(data)
     this.currentmodal=data;
     console.log(this.currentmodal);
      if(data=="1"){
        this.consumerhidden=true;
        this.motorhidden = false;
      }
      if(data=="2"){
        this.motorhidden = true;
        this.consumerhidden=false;
      }
     //  if(data=="3"){
     //   this.motorhidden = false;
     //   this.consumerhidden=true;
     // }
     // if(data=="4"){
     //   this.motorhidden = false;
     //   this.consumerhidden=true;
     // }
    }
 
    addproduct(){

        // localStorage.setItem('Manufacturer',JSON.stringify(this.addprodetail));
        // this.addproductlist = JSON.parse(localStorage.getItem('Manufacturer'));
        // categoryid,productname,referenceno,dateofpurchase,manufacturer,modelname,modelyear,vin,retailername,paymentmode,amount,createtype

                          // console.log(this.categoryid);
                          // var year = this.modelyear
                          var year = moment(this.modelyear).format('YYYY');
                          var dateofpurchaseinmanufucturer = moment(this.dateofpurchase).format('YYYY-MM-DD')
                          // var dateofpurchaseinmanufucturer = moment(this.dateofpurchase).format('YYYY-MM-DD');
                          console.log(year);
                          console.log(dateofpurchaseinmanufucturer);
        this.userService.addProduct(0,this.mid,this.currentmodal,this.productname,this.referenceno,dateofpurchaseinmanufucturer,this.Manufacturer,this.modelname,year,this.vinfrom,this.vinto,this.retailername,this.paymentmode,this.payment,2)
        .pipe(first()).subscribe((res:any)=>{
            console.log(res);
           this.productlistadd();
        });
}








   viewcomplaintClick(){
    this.router.navigate(['/viewcomplaint']);
   }
   


productlistadd(){
  this.userService.listproduct(0,this.mid).subscribe((res:any)=>{
    this.productlist=res.payload.user;
    console.log(res);
  });
}   


 }
 