import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ManageProductComponent } from './manage-product/manage-product.component';
import { RecallComponent } from './recall/recall.component';
import { RecallWebComponent } from './recall-web/recall-web.component';
import { AdminmanageProductComponent } from './adminmanage-product/adminmanage-product.component';
import { RecallStatusComponent } from './recall-status/recall-status.component';
import { SupplierRegisterComponent } from './supplier-register/supplier-register.component';
import { SettingsComponent } from './settings/settings.component';
import { LoyaltypointsComponent } from './loyaltypoints/loyaltypoints.component';
import { AdminrecallprocessComponent } from './adminrecallprocess/adminrecallprocess.component';
import { ViewcompliantComponent } from './viewcompliant/viewcompliant.component';
import { NotificationComponent } from './notification/notification.component';
import { ManufacturerLoginComponent } from './manufacturer-login/manufacturer-login.component';
import { ReportAProblemComponent } from './report-aproblem/report-aproblem.component';
import { MessageComponent } from './message/message.component';
import { ManageretailerComponent } from './manageretailer/manageretailer.component';
import { ManuctfurerdashboardComponent } from './manuctfurerdashboard/manuctfurerdashboard.component';
import { ManufactureraddrecallComponent } from './manufactureraddrecall/manufactureraddrecall.component';
import { RecalllistComponent } from './recalllist/recalllist.component';
import { AddmotorrecallComponent } from './addmotorrecall/addmotorrecall.component';
import { SubretailerComponent } from './subretailer/subretailer.component';
import { ManufuctrerrecallstatusComponent } from './manufuctrerrecallstatus/manufuctrerrecallstatus.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { ReportsComponent } from './reports/reports.component';
import { ManagesaleComponent } from './managesale/managesale.component';
import { ManufurersingupComponent } from './manufurersingup/manufurersingup.component';
import { RecalldetailComponent } from './recalldetail/recalldetail.component';
import { ManufucturersettingsComponent } from './manufucturersettings/manufucturersettings.component';

const routes: Routes = [
  { path:'',component:LoginComponent},
  { path:'login',component:LoginComponent},
  { path:'dashboard',component:DashboardComponent},
  { path:'register',component:RegisterComponent},
  { path:'manage-product',component:ManageProductComponent},
  { path:'recall-web',component:RecallWebComponent},
  { path:'recall',component:RecallComponent},
  { path:'manufuctrermanage-product', component: AdminmanageProductComponent},
  { path:'recall-status',component:RecallStatusComponent },
  { path:'supplier-register',component:SupplierRegisterComponent},
  { path:'settings',component:SettingsComponent},
  { path:'loyaltypoints',component:LoyaltypointsComponent},
  { path:'adminrecall-process',component:AdminrecallprocessComponent},
  { path:'manageretailer',component:ManageretailerComponent},
  { path:'subretailer',  component:SubretailerComponent },
  { path:'manufacturer-login',component:ManufacturerLoginComponent},
  { path:'viewcomplaint',component:ViewcompliantComponent},
  { path:'message',  component:MessageComponent},
  { path:'notification',component:NotificationComponent },
  { path:'report-aproblem',component:ReportAProblemComponent},
  { path:'manufacturerdashboard', component:ManuctfurerdashboardComponent},
  { path:'addrecall', component:ManufactureraddrecallComponent},
  { path:'recalllist', component:RecalllistComponent},
  { path:'addrecallmotor', component:AddmotorrecallComponent},
  { path:'managemyrecall',component:ManufuctrerrecallstatusComponent},
  { path:'purchase', component:PurchaseComponent},
  { path:'reports', component:ReportsComponent},
  { path:'sale',component:ManagesaleComponent},
  { path:'manufacturerregister',component:ManufurersingupComponent},
  { path:'recall-detail',component:RecalldetailComponent},
  { path:'manufucturer-settings',component:ManufucturersettingsComponent}
];

@NgModule({
  imports: 
  [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
