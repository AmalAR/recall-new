import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ManageProductComponent} from './manage-product/manage-product.component';
// import { ManageProductComponent , ReportDialog} from './manage-product/manage-product.component';
import { RecallComponent } from './recall/recall.component';
import { RecallWebComponent } from './recall-web/recall-web.component';
import { AdminmanageProductComponent } from './adminmanage-product/adminmanage-product.component';
import { RecallStatusComponent } from './recall-status/recall-status.component';
import { SupplierRegisterComponent } from './supplier-register/supplier-register.component';
import { SettingsComponent } from './settings/settings.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoyaltypointsComponent } from './loyaltypoints/loyaltypoints.component';
import { ManageretailerComponent } from './manageretailer/manageretailer.component';
import { AdminrecallprocessComponent } from './adminrecallprocess/adminrecallprocess.component';
import { ManufacturerLoginComponent } from './manufacturer-login/manufacturer-login.component';
import { ViewcompliantComponent } from './viewcompliant/viewcompliant.component';
import { NotificationComponent } from './notification/notification.component';
import { ReportAProblemComponent } from './report-aproblem/report-aproblem.component';
import { MessageComponent } from './message/message.component';
import { RouterModule, Routes } from '@angular/router';
import { ManuctfurerdashboardComponent } from './manuctfurerdashboard/manuctfurerdashboard.component';
import { ManufactureraddrecallComponent } from './manufactureraddrecall/manufactureraddrecall.component';
import { RecalllistComponent } from './recalllist/recalllist.component';
import { AddmotorrecallComponent } from './addmotorrecall/addmotorrecall.component';
import { SubretailerComponent } from './subretailer/subretailer.component';
import { ManufuctrerrecallstatusComponent } from './manufuctrerrecallstatus/manufuctrerrecallstatus.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { ReportsComponent } from './reports/reports.component';
import { HttpClientModule } from '@angular/common/http';
import { ManagesaleComponent } from './managesale/managesale.component';
import { ManufurersingupComponent } from './manufurersingup/manufurersingup.component';
import { RecalldetailComponent } from './recalldetail/recalldetail.component';
import { ManufucturersettingsComponent } from './manufucturersettings/manufucturersettings.component';
// import { UserService } from './user.service';


const routes: Routes = [];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DashboardComponent,
    ManageProductComponent,
    // ReportDialog,
    RecallComponent,
    DashboardComponent,
    RecallWebComponent,
    LoyaltypointsComponent,
    RecallWebComponent,
    RecallStatusComponent,
    SupplierRegisterComponent,
    SettingsComponent,

    ManageretailerComponent,
    AdminrecallprocessComponent,

    AdminmanageProductComponent,
    ManufacturerLoginComponent,

    ReportAProblemComponent,
    ViewcompliantComponent,
    AdminmanageProductComponent,

    NotificationComponent,
    ManufacturerLoginComponent,

    MessageComponent,

    ManuctfurerdashboardComponent,

    ManufactureraddrecallComponent,

    RecalllistComponent,

    AddmotorrecallComponent,

    SubretailerComponent,

    ManufuctrerrecallstatusComponent,

    PurchaseComponent,

    ReportsComponent,

    ManagesaleComponent,

    ManufurersingupComponent,

    RecalldetailComponent,

    ManufucturersettingsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    HttpClientModule,
    MatTreeModule,BrowserAnimationsModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  providers: [],
  // providers: [UserService],
  bootstrap: [AppComponent ,
    //  ReportDialog
    ]
})
export class AppModule { }
