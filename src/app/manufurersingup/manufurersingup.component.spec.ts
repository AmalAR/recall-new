import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManufurersingupComponent } from './manufurersingup.component';

describe('ManufurersingupComponent', () => {
  let component: ManufurersingupComponent;
  let fixture: ComponentFixture<ManufurersingupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManufurersingupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManufurersingupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
