import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { FormGroup, FormControl, Validators, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Location } from '@angular/common';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public  loginbg="loginbg.jpg"
  public logo="logolog.png"
  hide = true;
  mname:any;
  fsttname:any;
  lstname:any;
  pswrd:any;
  cpswrd:any;
  checkit:any;
  newuserreg=[];
  registering: any[];
  signup: FormGroup;
  constructor(
    public router:Router,
    public userServices:UserService,
    private fb:FormBuilder,
    public location:Location
  ) { }

  ngOnInit() {
    this.createform();
  }
  loginClick(){
  this.router.navigate(['/login']);
  }



  private createform(){
    this.signup=new FormGroup({
      firstname:new FormControl('',[Validators.required]),
      lastname:new FormControl('',[Validators.required]),
      email:new FormControl('',[Validators.required,Validators.email]),
      password:new FormControl('',Validators.required),
      confirmpassword:new FormControl('',[Validators.required,this.equalto('password')]),
    });
  }

      //password validation
  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
    
    let input = control.value;
    
    let isValid=control.root.value[field_name]==input
    if(!isValid) 
    return { 'equalTo': {isValid} }
    else 
    return null;
    };
    }











  registerit(){
    // this.registering=[{
    //   mname: this.mname,
    //    fsttname:this.fsttname,
    //    lstname: this.lstname,
    //    pswrd:this.pswrd,
    //    cpswrd:this.cpswrd,
    //    checkit:this.checkit
    // }];
    
    // console.log(this.fsttname);
this.userServices.RegisterAPI(this.signup.value.firstname,this.signup.value.lastname,this.signup.value.email,this.signup.value.confirmpassword,1).pipe(first()).subscribe((res:any)=>{
  console.log(res);
});


    // this.newuserreg.push(this.registering[0]);
    // localStorage.setItem('newregister',JSON.stringify(this.newuserreg));
  }



}
