import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManufucturersettingsComponent } from './manufucturersettings.component';

describe('ManufucturersettingsComponent', () => {
  let component: ManufucturersettingsComponent;
  let fixture: ComponentFixture<ManufucturersettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManufucturersettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManufucturersettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
