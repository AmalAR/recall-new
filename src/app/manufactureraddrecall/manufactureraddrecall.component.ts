import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { UserService } from '../user.service';
import { first } from 'rxjs/operators';
import * as moment from 'moment';
@Component({
  selector: 'app-manufactureraddrecall',
  templateUrl: './manufactureraddrecall.component.html',
  styleUrls: ['./manufactureraddrecall.component.scss']
})
export class ManufactureraddrecallComponent implements OnInit {
  addrecallform:FormGroup;
  userinfo: any;
  mid: any;
  constructor(
    public router:Router,
    private fb:FormBuilder,
    public userServices:UserService
  ) { }

  ngOnInit() {
    this.createform();
  }

private createform(){
  this.addrecallform = new FormGroup({
    recallno:new FormControl('',[Validators.required]),
    productname:new FormControl('',[Validators.required]),
    recalldate:new FormControl('',[Validators.required]),
    summary:new FormControl('',[Validators.required]),
    reason:new FormControl('',[Validators.required]),
    remedy:new FormControl('',[Validators.required]),
    units:new FormControl('',[Validators.required]),
    productphotos:new FormControl('',[Validators.required]),
    consumercontact:new FormControl('',[Validators.required]),
    description:new FormControl('',[Validators.required]),
    injuries:new FormControl('',[Validators.required]),
    soldat:new FormControl('',[Validators.required]),
    manufacturers:new FormControl('',[Validators.required]),
    importers:new FormControl('',[Validators.required]),
    distributors:new FormControl('',[Validators.required]),
    manufacturedin:new FormControl('',[Validators.required]),
    notes:new FormControl('',[Validators.required]),
    vin:new FormControl('',[Validators.required]),
  })
}



  AddRecallClick(){
    var recaldateformat = moment(this.addrecallform.value.recalldate).format('YYYY-MM-DD')
    console.log(recaldateformat);
    this.userinfo=JSON.parse(localStorage.getItem('userinfo'));
   
    console.log(this.userinfo);
    this.mid=this.userinfo.id;
    console.log("manuid", this.mid);
    this.userServices.addProductRecall(this.mid,this.addrecallform.value.recallno,this.addrecallform.value.productname,recaldateformat,this.addrecallform.value.summary,this.addrecallform.value.reason,this.addrecallform.value.remedy,this.addrecallform.value.units,this.addrecallform.value.productphotos,
this.addrecallform.value.consumercontact,this.addrecallform.value.description,this.addrecallform.value.injuries,
this.addrecallform.value.soldat,this.addrecallform.value.manufacturers,this.addrecallform.value.importers,this.addrecallform.value.distributors,this.addrecallform.value.manufacturedin,this.addrecallform.value.notes,this.addrecallform.value.vin
      ).pipe(first()).subscribe((res:any)=>{
     console.log(res)
     this.router.navigate(['/recall']);
    });

   }
  
}
