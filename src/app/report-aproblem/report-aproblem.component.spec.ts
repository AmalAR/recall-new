import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportAProblemComponent } from './report-aproblem.component';

describe('ReportAProblemComponent', () => {
  let component: ReportAProblemComponent;
  let fixture: ComponentFixture<ReportAProblemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportAProblemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportAProblemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
