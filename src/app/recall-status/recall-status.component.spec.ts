import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecallStatusComponent } from './recall-status.component';

describe('RecallStatusComponent', () => {
  let component: RecallStatusComponent;
  let fixture: ComponentFixture<RecallStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecallStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecallStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
