import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubretailerComponent } from './subretailer.component';

describe('SubretailerComponent', () => {
  let component: SubretailerComponent;
  let fixture: ComponentFixture<SubretailerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubretailerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubretailerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
