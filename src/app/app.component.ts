import { Component,OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public userprofile ="profile_user.jpg";
  public manulogo = 'logo_manu.png'
  public recalllogo ="logo.png"
  route: string;
  profilename: any;
  constructor(
    public router:Router,
    location:Location,
    private routeParam: ActivatedRoute
  ) {
        // Set the defaults
        // Set the private defaults
        router.events.subscribe((val) => {
            console.log(this.route = location.path())
            // if(location.path() != ''){
            //   this.route = location.path();
            // } else {
            //   this.route = 'Home'
            // }
          });
   }

  ngOnInit() {
    this.profilename = JSON.parse(localStorage.getItem('userinfo'));
    console.log(this.profilename);
  }
  title = 'recall';
  logoutClick(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }
  registerClick(){
    this.router.navigate(['/register']);
  }
  logintohome(){
    this.router.navigate(['/dashboard'])  
    // window.location.reload();
  } 
  settingsClick(){
    this.router.navigate(['/settings'])  
  }
  manufecturerloginClick(){
    this.router.navigate(['/manufacturer-login']);
    }
    manufecturerregisterClick(){
      this.router.navigate(['/supplier-register']);
  }
  recallClick(){
    this.router.navigate(['/recall']);
    // window.location.reload();
  }
  manageproductClick(){
    this.router.navigate(['/manage-product']); 
  }
  manucfturermangesaleClick(){
    this.router.navigate(['/sale']);
  }
  myrecallClick(){
    this.router.navigate(['/recall-web']);
    // window.location.reload(); 
  }
  mypurchaseClick(){
    this.router.navigate(['/purchase']);
    // window.location.reload();
  }
  loyaltyClick(){
    this.router.navigate(['/loyaltypoints']); 
    // window.location.reload();
  }
  notificationClick(){
    this.router.navigate(['/notification']); 
  }
  ReportaproblemClick(){
    this.router.navigate(['/report-aproblem']); 
    // window.location.reload();
  }  

  manucfturerhome(){
    this.router.navigate(['/manufacturerdashboard']);  
  }
  manucfturermangeProductClick(){
    this.router.navigate(['/manufuctrermanage-product']);  
  }
  manageretailerClick(){
    this.router.navigate(['/manageretailer']);  
    // window.location.reload();
  }
  viewcomplaintClick(){
    this.router.navigate(['/viewcomplaint']); 
    // window.location.reload(); 
  }
reportClick(){
  this.router.navigate(['/reports']); 
}
manusettingsClick(){
  this.router.navigate(['/manufucturer-settings']); 
}
}
