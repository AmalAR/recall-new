import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-recalldetail',
  templateUrl: './recalldetail.component.html',
  styleUrls: ['./recalldetail.component.scss']
})
export class RecalldetailComponent implements OnInit {
  userdetail: any;
  openrecall: any;
  recalldetail=[];

  constructor(
    public userService:UserService 
  ) { }

  ngOnInit() {
    this.userdetail=JSON.parse(localStorage.getItem("userinfo"));
    this.userService.listRecallOpen(this.userdetail.id).pipe(first()).subscribe((res:any)=>{
      this.openrecall=res.payload.user;
      console.log(this.openrecall);
      const productid = JSON.parse(localStorage.getItem('productid'));
      for(var i=0;i<this.openrecall.length;i++)
      {
        if(this.openrecall[i].id==productid){
          console.log(this.openrecall[i].id);
           this.recalldetail.push(this.openrecall[i]);
        }
      }
      console.log(this.recalldetail);

})
  }

}
