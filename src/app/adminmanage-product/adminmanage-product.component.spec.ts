import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminmanageProductComponent } from './adminmanage-product.component';

describe('AdminmanageProductComponent', () => {
  let component: AdminmanageProductComponent;
  let fixture: ComponentFixture<AdminmanageProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminmanageProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminmanageProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
