import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { first } from 'rxjs/operators';



export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];


@Component({
  selector: 'app-recall',
  templateUrl: './recall.component.html',
  styleUrls: ['./recall.component.scss']
})
export class RecallComponent implements OnInit {

  @ViewChild(MatPaginator, {}) paginator: MatPaginator;
  @ViewChild(MatSort, {}) sort: MatSort;



  displayedColumns: string[] = ['id', 'name', 'progress', 'color'];
  dataSource: MatTableDataSource<UserData>;

  recallcards:any;
  
  public manulogo = 'ford-logo.png'
  recentrecalls: any;
  mobilelist:any;
  cemerapart=false;
  mobilepart=false;
  currentproduct:any;
  manufurerinfo: any;
  maufucturerid: any;
  recalllistdetail: any;
  constructor(
    public router:Router,
    public userService:UserService
  ) {

    // Create 100 users
    const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);


    this.manufurerinfo=JSON.parse(localStorage.getItem('userinfo'));
   
    console.log(this.manufurerinfo);
    this.maufucturerid=this.manufurerinfo.id;
    console.log("userid",this.maufucturerid);
    this.userService.listRecall(this.maufucturerid).pipe(first()).subscribe((res:any)=>{
   console.log(res);
   this.recalllistdetail = res.payload.user;
   console.log(this.recalllistdetail);
    });
   }

  ngOnInit() {

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;


    this.recentrecalls = [
      {recentproductname:"Camera",producttype:'Camera',recentproductcount:17,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ] },
      {recentproductname:"Watch",producttype:'Watch',recentproductcount:50,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ] },
      {recentproductname:"Mobiles",producttype:'Mobiles',recentproductcount:25,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ] },
      {recentproductname:"BRANDS",recentproductcount:10,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ] },
      {recentproductname:"OFFERS",recentproductcount:15,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ]},
      {recentproductname:"CATEGORIES",recentproductcount:35,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."}           
      ] }
]


  this.recallcards = [
    {rating:"4.8",productimg:"cemra1.jpeg",producttype:"Canon M50 Mirrorless",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",shippingdate:"16-05-2019"},
    {rating:"4.9",productimg:"cemra2.jpeg",producttype:"Canon EOS 200D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",shippingdate:"16-05-2019"},
    {rating:"4.0",productimg:"cemra3.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",shippingdate:"16-05-2019"},
    {rating:"3.8",productimg:"cemra4.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",shippingdate:"16-05-2019"},
    {rating:"3.8",productimg:"cemra1.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",shippingdate:"16-05-2019"},
    {rating:"3.8",productimg:"cemra2.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",shippingdate:"16-05-2019"},
  ]
  
  // this.mobilelist = [
  //   {rating:"4.8",productimg:"mobile.jpeg",producttype:"Canon M50 Mirrorless",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",color:"Black",freeshoping:"500"},
  //   {rating:"4.9",productimg:"mobile.jpeg",producttype:"Canon EOS 200D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",color:"Grey",freeshoping:"500"},
  //   {rating:"4.0",productimg:"mobile.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",color:"White",freeshoping:"1500"},
  //   {rating:"3.8",productimg:"mobile.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",color:"Red",freeshoping:"2500"},
  //   {rating:"3.8",productimg:"mobile.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",color:"Black",freeshoping:"500"},
  //   {rating:"3.8",productimg:"mobile.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",color:"Black",freeshoping:"500"},
  //   {rating:"3.8",productimg:"mobile.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",color:"Black",freeshoping:"500"},
  //   {rating:"3.8",productimg:"mobile.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",color:"Black",freeshoping:"500"},
  //   {rating:"3.8",productimg:"mobile.jpeg",producttype:"Canon 1300D DSLR",recallprice:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card. If you are using 2 SIM cards",color:"Black",freeshoping:"1000"},
  // ]
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }



  productClick(data){
    alert(data)
    this.currentproduct = data;
     if(this.currentproduct=="Camera"){
      this.cemerapart=true;
      this.mobilepart=false;
     }
     if(this.currentproduct=="Mobiles"){
      this.cemerapart=false;
      this.mobilepart=true;
     }     
  }
  addnewrecallClick(){
   this.router.navigate(['/addrecall']);
  }

  PublishClick(publishid){
    console.log(publishid);
    alert(publishid)
    this.userService.publishRecall(publishid).pipe(first()).subscribe((res:any)=>{
      console.log(res);
    });
    
  }

}


/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    id: id.toString(),
    name: name,
    progress: Math.round(Math.random() * 100).toString(),
    color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
  };
}