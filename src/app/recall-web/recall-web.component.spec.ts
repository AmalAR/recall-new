import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecallWebComponent } from './recall-web.component';

describe('RecallWebComponent', () => {
  let component: RecallWebComponent;
  let fixture: ComponentFixture<RecallWebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecallWebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecallWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
