import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addmotorrecall',
  templateUrl: './addmotorrecall.component.html',
  styleUrls: ['./addmotorrecall.component.scss']
})
export class AddmotorrecallComponent implements OnInit {

  constructor(
    public router:Router
  ) { }

  ngOnInit() {
  }
  AddRecallClick(){
    this.router.navigate(['/recalllist']);
   }
}
