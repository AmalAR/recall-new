import { Component, OnInit,ViewChild } from '@angular/core';
import {MatTableDataSource,MatSort,MatPaginator} from '@angular/material';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
export interface PeriodicElement {
  date: string;
  position: number;
  productname: string;
  sellername: string;
  compliant:string;
}


const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, date: '11/05/2019', productname: "Camera", sellername: "john",compliant:'This is Product Very Slow'},
  {position: 2, date: '11/05/2019', productname: "Mobile", sellername: "wick",compliant:'This is Product Very Slow'},
  {position: 4, date: '11/05/2019', productname: "Camera", sellername: "stannis",compliant:'This is Product Very Slow'},
  {position: 5, date: '11/05/2019', productname: "Watch", sellername: "renly",compliant:'This is Product Very Slow'},
  {position: 6, date: '11/05/2019', productname: "Mobile", sellername: "stannis",compliant:'This is Product Very Slow'},
  {position: 7, date: '11/05/2019', productname: "Watch", sellername: "wick",compliant:'This is Product Very Slow'},
  {position: 8, date: '11/05/2019', productname: "Mobile", sellername: "wick",compliant:'This is Product Very Slow'},
];

@Component({
  selector: 'app-viewcompliant',
  templateUrl: './viewcompliant.component.html',
  styleUrls: ['./viewcompliant.component.scss']
})
export class ViewcompliantComponent implements OnInit {
  recentrecalls: any[];
  recallcards: {}[];

  public manulogo = 'ford-logo.png'
  displayedColumns: string[] = ['position', 'date', 'productname', 'compliant','sellername','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(public router:Router) { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    this.recallcards = [
      {recallicon:"insert_chart_outlined",recallcount:"1400",recalltitle:"Previous",iconsclass:"blue_bg"},
      {recallicon:"insert_chart_outlined",recallcount:"400",recalltitle:"New",iconsclass:"voilet_bg"},
      {recallicon:"insert_chart_outlined",recallcount:"1800",recalltitle:"Total",iconsclass:"yellow_bg"},
    ]


  }


  chat(){
    this.router.navigate(['/message']);
  }

  removeComplaint(){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Message has been deleted.',
          'success'
        )
      }
    })

  }



}
