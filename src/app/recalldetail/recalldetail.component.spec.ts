import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecalldetailComponent } from './recalldetail.component';

describe('RecalldetailComponent', () => {
  let component: RecalldetailComponent;
  let fixture: ComponentFixture<RecalldetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecalldetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecalldetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
