import { Component, OnInit,ViewChild } from '@angular/core';
import {MatTableDataSource,MatSort,MatPaginator} from '@angular/material';

export interface PeriodicElement {
  date: string;
  position: number;
  item: string;
  price: number;
  points:number;
}


const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, date: '11/05/2019', item: "Camera", price: 4200 , points: 400},
  {position: 2, date: '11/05/2019', item: "Mobile", price: 4200 , points: 400},
  {position: 4, date: '11/05/2019', item: "Camera", price: 4200, points: 400},
  {position: 5, date: '11/05/2019', item: "Watch", price: 4200, points: 400},
  {position: 6, date: '11/05/2019', item: "Mobile", price: 4200, points: 400},
  {position: 7, date: '11/05/2019', item: "Watch", price: 4200, points: 400},
  {position: 8, date: '11/05/2019', item: "Mobile", price: 4200, points: 400},
];




@Component({
  selector: 'app-loyaltypoints',
  templateUrl: './loyaltypoints.component.html',
  styleUrls: ['./loyaltypoints.component.scss']
})
export class LoyaltypointsComponent implements OnInit {

  recentrecalls: any[];
  recallcards: {}[];
  newsupdatelist: any[];
  newsupdatelistconsumer:any;
  newsupdatelistfood:any;
  newsupdatelistdrug:any;

  displayedColumns: string[] = ['position', 'date', 'item', 'price','points'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor() { }

  ngOnInit() {

    this.recentrecalls = [
      {recentproductname:"Camera",producttype:'Camera',recentproductcount:17,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ] },
      {recentproductname:"Watch",producttype:'Watch',recentproductcount:50,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ] },
      {recentproductname:"Mobiles",producttype:'Mobiles',recentproductcount:25,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ] },
      {recentproductname:"BRANDS",recentproductcount:10,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ] },
      {recentproductname:"OFFERS",recentproductcount:15,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."},
      ]},
      {recentproductname:"CATEGORIES",recentproductcount:35,producttags:[
        {subproducts:"The second SIM slot of this dual SIM phone accepts either a nano SIM card or a microSD Memory Card."}           
      ] }
];

this.newsupdatelist = [
  {newstitle:'MITSUBISHI ( 19V330000 )',newscontent:'Dated: APR 26, 2019 Mitsubishi Motors North Americ...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V330000&refurl=rss'},
  {newstitle:'HONDA ( 19V315000 )',newscontent:'Dated: APR 18, 2019 Honda (American Honda Motor Co...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V315000&refurl=rss'},
  {newstitle:'VOLVO ( 19V327000 )',newscontent:'Dated: APR 25, 2019 Volvo Car USA LLC (Volvo) is r...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V327000&refurl=rss'},
  {newstitle:'FREIGHTLINER ( 19V309000 )',newscontent:'Dated: MAY 10, 2019 Forest River, Inc. (Forest Riv...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V309000&refurl=rss'},
  {newstitle:'LIVI\' LITE ( 19V307000 )',newscontent:'Dated: APR 17, 2019 Livin\' Lite Recreational Vehic...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V327000&refurl=rss'},
  {newstitle:'LIVIN LITE ( 19V307000 )',newscontent:'Dated: APR 17, 2019 Livin\' Lite Recreational Vehic...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V307000&refurl=rss'},
  {newstitle:'MAZDA ( 19V323000 )',newscontent:'Dated: APR 25, 2019 Mazda North American Operation...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V323000&refurl=rss'},
  {newstitle:'SPENCER ( 19V305000 )',newscontent:'Dated: APR 17, 2019 Spencer Manufacturing, Inc. is...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V305000&refurl=rss'},
  {newstitle:'UTILIMASTER ( 19V304000 )',newscontent:'Dated: APR 17, 2019 Spartan Motors USA (Spartan) i...',path:'https://www.nhtsa.gov/recalls?nhtsaId=19V304000&refurl=rss'}
]
this.newsupdatelistconsumer = [
  {newstitle:'Target Recalls USB Charging Cables Due to Shock and Fire Hazards',newscontent:'The metal around the cord can become electrically ...',path:'https://www.cpsc.gov/Recalls/2019/target-recalls-usb-charging-cables-due-to-shock-and-fire-hazards'},
  {newstitle:'Aegean Apparel Recalls Children’s Sleepwear Due to Violation of Federal Flammability Standard',newscontent:'The children’s sleepwear garments fail to meet the...',path:'https://www.cpsc.gov/Recalls/2019/aegean-apparel-recalls-childrens-sleepwear-due-to-violation-of-federal-flammability'},
  {newstitle:'Children’s Sleep Sacks Recalled by Gildan Activewear Due to Violation of Federal Flammability Standard; Sold Exclusively at AmericanApparel.com (Recall Alert)',newscontent:'The children’s sleep sacks fail to meet the flamma...',path:'https://www.cpsc.gov/Recalls/2019/childrens-sleep-sacks-recalled-by-gildan-activewear-due-to-violation-of-federal'},
  {newstitle:'DICK’S Sporting Goods Recalls Ethos Pull-Up Assist Due To Laceration Hazard',newscontent:'The plastic clip on the nylon web band that is att...',path:'https://www.cpsc.gov/Recalls/2019/dicks-sporting-goods-recalls-ethos-pull-up-assist-due-to-laceration-hazard'},
  {newstitle:'Far East Brokers Recalls Pineapple Corer & Slicers Due to Laceration Hazard',newscontent:'The metal blade of the slicer can detach, posing a...',path:'https://www.cpsc.gov/Recalls/2019/far-east-brokers-recalls-pineapple-corer-and-slicers-due-to-laceration-hazard'},
  {newstitle:'Revelate Designs Recalls Bicycle Seat Bags Due to Crash, Injury Hazards',newscontent:'The straps that fasten the bag to the bike seat ca...',path:'https://www.cpsc.gov/Recalls/2019/revelate-designs-recalls-bicycle-seat-bags-due-to-crash-injury-hazards'},
  {newstitle:'Arctic Cat Recalls Textron Recreational Off-Highway Vehicles Due to Crash Hazard (Recall Alert)',newscontent:'The upper front suspension arms can fail, posing a...',path:'https://www.cpsc.gov/Recalls/2019/arctic-cat-recalls-textron-recreational-off-highway-vehicles-due-to-crash-hazard-recall'},
  {newstitle:'ION Audio Recalls Portable Speakers Due to Explosion Hazard',newscontent:'Hydrogen gas can leak from the portable speaker ba...',path:'https://www.cpsc.gov/Recalls/2019/ion-audio-recalls-portable-speakers-due-to-explosion-hazard'},
  {newstitle:'Meijer Recalls French Fry Cutters Due to Laceration Hazard',newscontent:'The french fry cutter’s blade can detach, posing a...',path:'https://www.cpsc.gov/Recalls/2019/meijer-recalls-french-fry-cutters-due-to-laceration-hazard'},
  {newstitle:'Beaba Recalls Baby Food Steam Cooker/Blenders Due to Laceration Hazard',newscontent:'The glass bowl can break, posing a laceration haza...',path:'https://www.cpsc.gov/Recalls/2019/Beaba-Recalls-Baby-Food-Steam-Cooker/Blenders-Due-to-Laceration-Hazard'}
]
this.newsupdatelistfood = [
  {newstitle:'J Deluca Fish Company Inc. Recalls Siluriformes Products Produced Without Benefit of Import Inspection',newscontent:'J Deluca Fish Company Inc., doing business as Naut...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-061-2019-release'},
  {newstitle:'Richwell Group, Inc. Recalls Siluriformes Products Produced Without Benefit of Import Inspection',newscontent:'Richwell Group, Inc. Recalls Siluriformes Products Produced Without Benefit of Import Inspection',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-060-2019-release'},
  {newstitle:'Sikorski Sausages Co., Ltd. Recalls Meat and Poultry Sausage Products Produced without Benefit of Import Inspection',newscontent:'Sikorski Sausages Co., Ltd., a London, Ontario, Ca...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-059-2019-release'},
  {newstitle:'Blount Fine Foods Recalls Soup with Chicken Products Due to Possible Foreign Matter Contamination',newscontent:'Blount Fine Foods, a McKinney, Texas establishment...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-058-2019-release'},
  {newstitle:'Aurora Packing Company, Inc. Recalls Beef Products Due to Possible E. coli O157:H7 Contamination',newscontent:'Aurora Packing Company, Inc., a North Aurora, Ill....',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-057-2019-release'},
  {newstitle:'BEF Foods, Inc. Recalls Beef Products Produced without Benefit of Inspection',newscontent:'BEF Foods, Inc., a Lima, Ohio establishment, is re...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-056-2019-release'},
  {newstitle:'Vienna Beef Ltd. Recalls Beef Products due to Possible Foreign Matter Contamination',newscontent:'Vienna Beef Ltd., a Chicago, Ill. establishment, i...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-055-2019-release'},
  {newstitle:'Caito Foods LLC. Recalls Salads with Chicken Products due to Misbranding and Undeclared Allergens',newscontent:'Caito Foods LLC., an Indianapolis, Ind. establish...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-055-2019-release'},
  {newstitle:'MIBO Fresh Foods LLC Recalls Salad with Meat Products due to Misbranding and Undeclared Allergens',newscontent:'MIBO Fresh Foods LLC, a Fort Worth, Texas establis...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-055-2019-release'},
  {newstitle:'Crab House Trading Corp. Recalls Siluriformes Products Produced without Benefit of Inspection',newscontent:'Crab House Trading Corp., a Los Angeles, Calif. es...',path:'https://www.fsis.usda.gov/wps/portal/fsis/topics/recalls-and-public-health-alerts/recall-case-archive/archive/2019/recall-055-2019-release'},
]
this.newsupdatelistdrug = [
  {newstitle:'E-cigarette: Safety Communication - Related to Seizures Reported Following E-cigarette Use, Particularly in Youth and Young Adults',newscontent:'Safety Communication - Related to Seizures Reporte...',path:'https://www.fda.gov/safety/medwatch-safety-alerts-human-medical-products/e-cigarette-safety-communication-related-seizures-reported-following-e-cigarette-use-particularly'},
  {newstitle:'Certain Prescription Insomnia Medicines: New Boxed Warning - Due to Risk of Serious Injuries Caused by Sleepwalking, Sleep Driving and Engaging in Other Activities While Not Fully Awake',newscontent:'MedWatch Alert on certain prescription insomnia me...',path:'https://www.fda.gov/safety/medwatch-safety-alerts-human-medical-products/certain-prescription-insomnia-medicines-new-boxed-warning-due-risk-serious-injuries-caused'}
]


    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    this.recallcards = [
      {recallicon:"insert_chart_outlined",recallcount:"1400",recalltitle:"Previous",iconsclass:"blue_bg"},
      {recallicon:"insert_chart_outlined",recallcount:"400",recalltitle:"New",iconsclass:"voilet_bg"},
      {recallicon:"insert_chart_outlined",recallcount:"1800",recalltitle:"Total",iconsclass:"yellow_bg"},
    ]

  }

}
