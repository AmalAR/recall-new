import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManufacturerLoginComponent } from './manufacturer-login.component';

describe('ManufacturerLoginComponent', () => {
  let component: ManufacturerLoginComponent;
  let fixture: ComponentFixture<ManufacturerLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManufacturerLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManufacturerLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
