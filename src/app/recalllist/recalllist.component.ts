import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recalllist',
  templateUrl: './recalllist.component.html',
  styleUrls: ['./recalllist.component.scss']
})
export class RecalllistComponent implements OnInit {

  products=[];
  
  currentmodal: any;
  viewproduct: any;
  constructor() { }

  ngOnInit() {
    this.products=[
      {
        "productid": 1,
        "procategory": "Food product",
        "procatemodel": "model1",
        "productheader" : "Motor Product",
        "ProductName" :"Motor Product",
        "Manufacturer" : "TVS",
        "ImporterWholesaler": "kya",
        "RetailerNameaddress" : "Anju, 45,Chennai ",
        "RetailerPhone" : "9823478634",
        "PurchaseDate" : "22-21-2009",
        "PaymentMode" : "Credit",
       },
       {
        "productid": 2,
        "procategory": "Motor vehicle",
        "procatemodel": "model2",
        "productheader" : "Consumer Product",
        "ProductName" :"Consumer Product",
        "Manufacturer" : "Hero",
        "ImporterWholesaler": "kya",
        "RetailerNameaddress" : "Anju, 45,Chennai ",
        "RetailerPhone" : "9823478634",
        "PurchaseDate" : "22-21-2009",
        "PaymentMode" : "Credit"
 
       
       },
       {
        "productid": 3,
        "procategory": "Consumer product",
        "productheader" : "consumer product",
        "procatemodel": "model3",
        "ProductName" : "Consumer Product",
        "Manufacturer" : "Bajaj",
        "ImporterWholesaler": "kya",
        "RetailerNameaddress" : "Anju, 45,Chennai ",
        "RetailerPhone" : "9823478634",
        "PurchaseDate" : "22-21-2009",
        "PaymentMode" : "Credit",
       },
       {
        "productid": 4,
        "procategory": "Drug",
        "procatemodel": "model4",
        "productheader" : "Motor Product",
        "ProductName" : "Motor Product",
        "Manufacturer" : "Honda",
        "ImporterWholesaler": "kya",
        "RetailerNameaddress" : "Kumar, 45,Bangalore ",
        "RetailerPhone" : "9823478634",
        "PurchaseDate" : "22-21-2009",
        "PaymentMode" : "Credit"
       },
 
   ]
 
  }

}
