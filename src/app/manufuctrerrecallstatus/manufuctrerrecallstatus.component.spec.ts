import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManufuctrerrecallstatusComponent } from './manufuctrerrecallstatus.component';

describe('ManufuctrerrecallstatusComponent', () => {
  let component: ManufuctrerrecallstatusComponent;
  let fixture: ComponentFixture<ManufuctrerrecallstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManufuctrerrecallstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManufuctrerrecallstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
