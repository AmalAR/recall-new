import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddmotorrecallComponent } from './addmotorrecall.component';

describe('AddmotorrecallComponent', () => {
  let component: AddmotorrecallComponent;
  let fixture: ComponentFixture<AddmotorrecallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddmotorrecallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddmotorrecallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
