import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http:HttpClient) { }



RegisterAPI(firstname,lastname,email,password,type){
  return this.http.post(`${environment.apiUrl}v1.0/user/createUser`,{firstname:firstname,lastname:lastname,email:email,password:password,type:type});
}
loginUser(username,password){
  return this.http.post(`${environment.apiUrl}v1.0/user/login`,{username:username,password:password});
}
addProduct(userid,mid,categoryid,productname,referenceno,dateofpurchase,manufacturer,modelname,modelyear,vin,vinto,retailername,paymentmode,amount,createtype){
  return this.http.post(`${environment.apiUrl}v1.0/user/addproduct`,{userid:userid,mid:mid,categoryid:categoryid,productname:productname,referenceno:referenceno,dateofpurchase:dateofpurchase,manufacturer:manufacturer,modelname:modelname,modelyear:modelyear,vin:vin,vinto:vinto,retailername:retailername,paymentmode:paymentmode,amount:amount,createtype:createtype});
}

// addProduct(userid,mid,categoryid,productname,referenceno,dateofpurchase,manufacturer,modelname,modelyear,vin,retailername,paymentmode,amount,createtype){
//   return this.http.post(`${environment.apiUrl}v1.0/user/addproduct`,{userid:userid,mid:mid,categoryid:categoryid,productname:productname,referenceno:referenceno,dateofpurchase:dateofpurchase,manufacturer:manufacturer,modelname:modelname,modelyear:modelyear,vin:vin,retailername:retailername,paymentmode:paymentmode,amount:amount,createtype:createtype});
// }
listCategorySelect(){
  return this.http.post(`${environment.apiUrl}v1.0/user/listCategorySelect`,{});
}
addProductRecall(mid,recallno,productname,recalldate,summary,reason,remedy,units,productphotos,consumer_contact,recalldescription,instant_injuries,soldat,manufacturer,importer,distributor,manufacturedin,notes,vin){
  return this.http.post(`${environment.apiUrl}v1.0/user/addProductRecall`,{mid:mid,recallno:recallno,productname:productname,recalldate:recalldate,summary:summary,
    reason:reason,remedy:remedy,units:units,productphotos:productphotos,consumer_contact:consumer_contact,recalldescription:recalldescription,
    instant_injuries:instant_injuries,soldat:soldat,manufacturer:manufacturer,importer:importer,distributor:distributor,manufacturedin:manufacturedin,notes:notes,
    vin:vin
    
  });
}

listproduct(userid,mid){
  return this.http.post(`${environment.apiUrl}v1.0/user/listproduct`,{userid:userid,mid:mid});
}
listRecall(id){
  return this.http.post(`${environment.apiUrl}v1.0/user/listRecall`,{id:id});
}
listRecallOpen(id){
  return this.http.post(`${environment.apiUrl}v1.0/user/listRecallOpen`,{userid :id});
}
publishRecall(id){
  return this.http.post(`${environment.apiUrl}v1.0/user/publishRecall`,{id:id});
}
listRecallClosed(id){
  return this.http.post(`${environment.apiUrl}v1.0/user/listRecallClosed`,{userid :id});
}
listRecallProgress(id){
  return this.http.post(`${environment.apiUrl}v1.0/user/listRecallProgress`,{userid :id});
}
 
}
